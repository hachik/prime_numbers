﻿using System.Numerics;

namespace PrimeNumbers;

public static class Primes
{
    private static BigInteger biSqrt(BigInteger n)
    {
        if (n == 0) return 0;
        if (n > 0)
        {
            int bitLength = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2)));
            BigInteger root = BigInteger.One << (bitLength / 2);

            while (!isSqrt(n, root))
            {
                root += n / root;
                root /= 2;
            }

            return root;
        }

        throw new ArithmeticException("NaN");
    }

    private static bool isSqrt(BigInteger n, BigInteger root)
    {
        BigInteger lowerBound = root * root;
        BigInteger upperBound = (root + 1) * (root + 1);

        return (n >= lowerBound && n < upperBound);
    }

    public static BigInteger RandomIntegerBelow(BigInteger N)
    {
        byte[] bytes = N.ToByteArray();
        BigInteger R;
        var rnd = new Random();
        do
        {
            rnd.NextBytes(bytes);
            bytes[bytes.Length - 1] &= (byte)0x7F; //force sign bit to positive
            R = new BigInteger(bytes);
        } while (R >= N);

        return R;
    }

    public static bool IsPrimeBrutforce(BigInteger n)
    {
        BigInteger i = 1;
        
        while (++i < n)
            if (n % i == 0)
                return false;

        return true;
    }

    public static bool IsPrimeMiller(BigInteger n/*, ulong? rounds = null*/)
    {
        // prepare rounds, log2(n) recommended and used as default
        //if (rounds is null)
        BigInteger rounds = BigInteger.Log2(n) + 1;

        // 6k+1 optimization
        if (n <= 3)
            return n >= 1;

        if (n == 5)
            return true;

        if ((n % 2 == 0) || (n % 3 == 0))
            return false;

        //present n as 2^s * t + 1
        BigInteger buff = n - 1;
        (BigInteger s, BigInteger t) = (0, 0);
        while (t == 0)
        {
            if (buff % 2 == 0)
            {
                buff /= 2;
                s += 1;
            }
            else
                t = buff;
        }

        var rnd = new Random();

        (BigInteger x, BigInteger y) = (0, 0);
        BigInteger a;
        for (BigInteger i = 0; i < rounds; i++)
        {
            a = RandomIntegerBelow(n - 3); // (ulong)rnd.NextInt64(2, n - 3);
            if (a < 2)
                a = 2;
            x = BigInteger.ModPow(a, t, n);
            for (ulong j = 0; j < BigInteger.Max(s, 1); j++)
            {
                y = BigInteger.ModPow(x, 2, n);
                if (y == 1 && x != 1 && x != n - 1)
                    return false;
                x = y;
            }
            if (y != 1)
                return false;
        }
        return true;
    }

    public static bool IsPrimeOptimized(BigInteger n)
    {
        if (n <= 3)
            return n >= 1;

        if ((n == 5) || (n == 7))
            return true;

        if ((n % 2 == 0) || (n % 3 == 0) || (n % 5 == 0))
            return false;

        BigInteger i = 5;

        var bisq = biSqrt(n);
        // use sqrt(n) instead of n in loop
        do
        {
            if (n % i == 0 || n % (i + 2) == 0)
                return false;
        }
        while ((i += 6) <= bisq);

        return true;
    }

    /*public static bool IsPrimeBrutforce(ulong num)
    {
        if (num <= 3)
            return num >= 1;

        if ((num % 2 == 0) || (num % 3 == 0))
            return false;
        if (num % 2 == 0)
            return false;

        double d;
        int r = 0;

        while ((num - 1) % Math.Pow(2, r + 1) == 0)
            r++;

        d = (num - 1) % Math.Pow(2, r);
        int[] a = { 2, 3, 5, 7, 11, 13, 17, 23, 31, 62, 73, 1662803 };
        bool primality = true;
        for (int k = 0; k < a.Length; k++)
        {
            if ((Math.Pow(a[k], d) - 1) % num != 0)
            {
                for (int s = 0; s < r - 1; s++)
                {
                    if ((Math.Pow(a[k], Math.Pow(2, s) * d) + 1) % num != 0)
                        primality = false;
                }

                //for (int j = 0; j < Math.Max(s - 1, 1); j++)
                //{
                //    x = PowMod(x, 2, n);
                //    if (x == n - 1)
                //       break;
                //}
                //if (i == rounds - 1)
                //    return false;

            }
        }
        return primality;
    }
    */
    public static List<BigInteger> GeneratePrimesListBrutforce(BigInteger max)
    {
        List<BigInteger> result = new();

        BigInteger n = 0;
        while (n++ < max)
            if (IsPrimeBrutforce(n))
                result.Add(n);

        return result;
    }

    public static List<BigInteger> GeneratePrimesListOptimized(BigInteger max)
    {
        List<BigInteger> result = new();

        BigInteger n = 0;
        while (n++ < max)
            if (IsPrimeOptimized(n))
                result.Add(n);

        return result;
    }

    public static List<BigInteger> GeneratePrimesListMiller(BigInteger max)
    {
        List<BigInteger> result = new();

        BigInteger n = 0;
        while (n++ < max)
            if (IsPrimeMiller(n))
                result.Add(n);

        return result;
    }

    public static BigInteger GetNextPrimeMiller(BigInteger start)
    {
        BigInteger n = 0;
        while (!IsPrimeMiller(start))
            start++;
                
        return start;
    }
}